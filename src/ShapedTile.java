/**
 * ShapedTile [Class43]
 * A shaped tile on the scene
 */
final class ShapedTile {
	public ShapedTile(int arg0, int arg1, int arg2, int arg3, int arg4, int arg5, boolean flag) {
		aBoolean721 = true;
		anInt716 = arg0;
		anInt717 = arg1;
		anInt718 = arg2;
		anInt719 = arg3;
		anInt720 = arg4;
		anInt722 = arg5;
		aBoolean721 = flag;
	}
	final int anInt716;
	final int anInt717;
	final int anInt718;
	final int anInt719;
	final int anInt720;
	boolean aBoolean721;
	final int anInt722;
}
