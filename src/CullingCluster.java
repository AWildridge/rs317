/**
 * CullingCluster [Class47]
 * A cluster of tile culling vars, thanks clienthax :)
 */
final class CullingCluster {
	CullingCluster() {
	}
	int tileStartX; // anInt787
	int tileEndX; // anInt788
	int tileStartY; // anInt789
	int tileEndY; // anInt790
	int searchMask; // anInt791
	int worldStartX; // anInt792
	int worldEndX; // anInt793
	int worldStartY; // anInt794
	int worldEndY; // anInt795
	int worldStartZ; // anInt796
	int worldEndZ; // anInt797
	int worldDistance; // anInt798
	int startXToScreen; // anInt799
	int endXToScreen; // anInt800
	int startYToScreen; // anInt801
	int endYToScreen; // anInt802
	int startZToScreen; // anInt803
	int endZToScreen; // anInt804
}
