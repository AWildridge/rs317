/**
 * WallDecoration [Object2]
 * Wall decor, i.e. vines
 */
public final class WallDecoration {
	public WallDecoration() {
	}
	int z;
	int x;
	int y;
	int orientation1;
	int orientation2;
	public Entity entity;
	public int uid;
	byte objectConfig;
}
