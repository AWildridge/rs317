/**
 * Vertex [Class33]
 * The average of surface normals for a vertex.
 */
final class VertexNormal {

	public VertexNormal() {
	}
	public int x;// anInt602
	public int y;// anInt603
	public int z;// anInt604
	public int magnitude;// anInt605
}
