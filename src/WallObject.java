/**
 * WallObject [Object1]
 * An object on a wall, i.e. shelves
 */
public final class WallObject {
	public WallObject() {
	}
	int z;
	int x;
	int y;
	int orientation1;
	int orientation2;
	public Entity aEntity_278;
	public Entity aEntity_279;
	public int uid;
	byte objectConfig;
}
